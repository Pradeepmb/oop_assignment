public class overloading{

     public static void main(String []args){
        System.out.println("Method Overloading");
        System.out.println("==================");
        polymorphism pm = new polymorphism();
        pm.poly(1,2);
        pm.poly(1,2,3);
        pm.poly("Hi");
     }
}

class polymorphism{
    public int poly(int a, int b){
        System.out.println("First Method Addition ==>" +a+b);
         System.out.println();
        return 0;
    }
    public int poly(int a, int b, int c){
        System.out.println("Second Method Addition ==>" +a+b+c);
         System.out.println();
        return 0;
    }
    public String poly(String a){
        System.out.println("Third Method ==>" +a);
         System.out.println();
        return null;
        
    }
}