public class overriding{

     public static void main(String []args){
        parent obj = new parent();
        parent obj1 = new child();
        obj.show();
        obj1.show();
                }
}

class parent{
    public void show(){
        System.out.println("I am a parent");
    }
}

class child extends parent{
    public void show(){
        System.out.println("I am a child");
    }
}