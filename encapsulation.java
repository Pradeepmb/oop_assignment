public class encapsulation{

     public static void main(String []args){
         
         car carobj = new car();
         carobj.setColor("Red");
         int i=10;
         carobj.setTyreSize(i);
         
         System.out.println("Accessing Public Variables");
         System.out.println("Car Name >>"+carobj.name);
         System.out.println("Car Capacity >>"+carobj.capacity);
         System.out.println();
         System.out.println();
         
         System.out.println("Accessing Private Variables");
         System.out.println("Car Tyre Size >> "+carobj.getTyresize());
         System.out.println("Car color >>"+ carobj.getColor());
         
       
          
     }
}


class car{
    
    private int tyreSize;
    private String color;
    
    public int capacity = 20;
    public String name = "Suzuki";
    
    public void setTyreSize(int tyreSize)
    {
        this.tyreSize = tyreSize;
        
    }
    
      public void setColor(String color)
      {
           this.color = color;
        
      }
      
      public String getColor()
      {
        return color; 
      }
      
      public int getTyresize()
      {
        return tyreSize;    
      }
}
