public class abstraction{

     public static void main(String []args){
        dog obj = new dog();
        obj.animalSound();
        obj.sleep();
     }
}

abstract class Animal {
  public abstract void animalSound();
  public void sleep() {
    System.out.println("I am Sleeping");
  }
}

class dog extends Animal {
  public void animalSound() {
    System.out.println("I am a DOG");
  }
}
