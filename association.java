public class association{

     public static void main(String []args){
       Forest forest = new Forest("Amazon"); 
        Animals animal = new Animals("Lion"); 
          
        System.out.println(animal.getAnimalName() +  
               " is Living in " + forest.getForestName()); 
     }
}

class Forest  
{ 
    private String name; 
      
    Forest(String name) 
    { 
        this.name = name; 
    } 
      
    public String getForestName() 
    { 
        return this.name; 
    } 
}  
  
class Animals 
{ 
    private String name; 
      
    Animals(String name)  
    { 
        this.name = name; 
    } 
      
    public String getAnimalName() 
    { 
        return this.name; 
    }  
} 